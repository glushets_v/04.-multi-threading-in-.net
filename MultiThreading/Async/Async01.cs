﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Async
{
    public class Async01
    {
        private int _sum = 0;
        private Object _flag = new Object();

        public async Task<int> Run(int end)
        {
            var taskList = new List<Task>();

            for (int i = 0; i < end; i++)
            {
                taskList.Add(AddInt(i+1));
            }

            await Task.WhenAll(taskList);

            Console.WriteLine(_sum);

            return _sum;
        }

        private async Task AddInt(int i)
        {
            lock (_flag)
            {
                _sum += i;
            }
        }
    }
}
