﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Async.Models;

namespace Async
{
    public class Async03
    {
        private List<Product> _sourceProducts;
        private Object _lockObj = new Object();

        public Async03()
        {
            _cart = new Cart();

            _sourceProducts = new List<Product>
            {
                new Product
                {
                    Name = "Bear",
                    Price = (decimal) 12.50
                },
                new Product
                {
                    Name = "Chips",
                    Price = (decimal) 10.05
                }
            };
        }

        public Cart _cart;

        public async Task<string> AddProduct(string name, int count)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }

            var product = _sourceProducts.FirstOrDefault(x => string.Equals(x.Name, name, StringComparison.OrdinalIgnoreCase));

            if (product == null)
            {
                var noProduct = $"No product with name - {name}";

                Console.WriteLine(noProduct);

                return noProduct;
            }

            _cart.Orders.Add(new Order { Count = count, Product = product });

            await RecalculateCart();

            Console.WriteLine(_cart.Sum);

            return "Success";
        }

        public async Task<string> RemoveProduct(string name, int count)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }

            var product = _sourceProducts.FirstOrDefault(x => string.Equals(x.Name, name, StringComparison.OrdinalIgnoreCase));

            if (product == null)
            {
                var noProduct = $"No product with name - {name}";

                Console.WriteLine(noProduct);

                return noProduct;
            }

            var order = _cart.Orders.FirstOrDefault(x => string.Equals(x.Product.Name, name, StringComparison.OrdinalIgnoreCase));

            Remove(order, count);

            await RecalculateCart();

            Console.WriteLine(_cart.Sum);

            return "Success";
        }

        private void Remove(Order order, int count)
        {
            if (order.Count <= count)
            {
                _cart.Orders.Remove(order);
            }
            else
            {
                _cart.Orders.Find(x => x == order).Count = count;
            }
        }

        private async Task RecalculateCart()
        {
            lock (_lockObj)
            {
                _cart.Sum = _cart.Orders.Sum(x => x.OrderSum);
            }
        }
    }
}
