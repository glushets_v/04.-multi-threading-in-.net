﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Async.Models;

namespace Async
{
    public class Async04
    {
        public Async04()
        {
            _users = new List<User>();
        }

        public List<User> _users;

        public async Task AddAsync(User user)
        {
            _users.Add(user);
        }

        public async Task<User> GetByIdAsync(int id)
        {
            return _users.FirstOrDefault(x => x.Id == id);
        }

        public async Task<bool> UpdateAsync(User updateUser)
        {
            var storedUser = _users.FirstOrDefault(x => x.Id == updateUser.Id);

            if (storedUser == null)
            {
                return false;
            }

            storedUser.Age = updateUser.Age;
            storedUser.FirstName = updateUser.FirstName;
            storedUser.LastName = updateUser.LastName;

            return true;
        }

        public async Task<bool> DeleteUser(int id)
        {
            var storedUser = _users.FirstOrDefault(x => x.Id == id);

            if (storedUser == null)
            {
                return false;
            }

            _users.Remove(storedUser);

            return true;
        }
    }
}
