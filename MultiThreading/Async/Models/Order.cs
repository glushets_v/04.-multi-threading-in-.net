﻿namespace Async.Models
{
    public class Order
    {
        public Product Product { get; set; }

        public int Count { get; set; }

        public decimal OrderSum => Product.Price * Count;
    }
}