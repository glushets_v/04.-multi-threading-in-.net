﻿using System.Collections.Generic;

namespace Async.Models
{
    public class Cart
    {
        public Cart()
        {
            Orders = new List<Order>();
        }

        public List<Order> Orders { get; set; } 

        public decimal Sum { get; set; }
    }
}
