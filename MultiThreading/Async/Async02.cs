﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Async
{
    public class Async02
    {
        private CancellationTokenSource cts;

        public async Task<List<string>> LoadPages(string[] urls)
        {
            Task<string>[] tasks = null;
            cts = new CancellationTokenSource();
            try
            {
                tasks = urls.Select(url => LoadPage(url, cts.Token)).ToArray();

                await Task.WhenAll(tasks);
            }
            catch (OperationCanceledException ex)
            {
                Console.WriteLine("Loading canceled");
            }

            if (tasks == null)
            {
                return null;
            }

            var results = tasks.Select(x => x.Result).ToList();

            results.ForEach(Console.WriteLine);

            return results;
        }

        public void Cancel()
        {
            if (cts != null)
            {
                cts.Cancel();
            }
        }

        private async Task<string> LoadPage(string url, CancellationToken ct)
        {
            using (var client = new HttpClient())
            {
                HttpResponseMessage responce = await client.GetAsync(url, ct);

                return await responce.Content.ReadAsStringAsync();
            }
        }
    }
}
