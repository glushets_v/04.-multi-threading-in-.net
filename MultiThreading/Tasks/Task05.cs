﻿using System;
using System.Threading;

namespace Tasks
{
    public class Task05
    {
        private int counter = 0;
        private int threadCount = 10;

        private readonly Semaphore sem = new Semaphore(1, 10); 

        public void Run()
        {
            ThreadPool.QueueUserWorkItem(state => SomeDo(10));
        }

        private int SomeDo(int state)
        {
            sem.WaitOne();

            counter++;

            if (state == 0)
            {
                return 0;
            }

            var temp = --state;

            if (counter <= threadCount)
            {
                ThreadPool.QueueUserWorkItem(stateT => SomeDo(temp));
            }

            Console.WriteLine(temp);
            sem.Release();

            return temp;
        }
    }
}
