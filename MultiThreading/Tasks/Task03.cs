﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Tasks
{
    public class Task03
    {
        private Random _random;

        public Task03()
        {
            _random = new Random();
        }

        public void Run()
        {
            var matrice1 = GetRandomMatrice();
            var matrice2 = GetRandomMatrice();

            var matriceResult = new List<List<int>>();

            Parallel.For(0, matrice1.Count, (i, state) =>
            {
                var resultRow = new List<int>();

                for (int j = 0; j < matrice1[i].Count; j++)
                {
                    resultRow.Add(matrice1[i][j] * matrice2[i][j]);
                }

                matriceResult.Add(resultRow);
            });

            Console.WriteLine("Matrice1");
            matrice1.ForEach(row =>
            {
                row.ForEach(x => Console.Write("{0}, ", x));
                Console.WriteLine();
            });

            Console.WriteLine();
            Console.WriteLine("Matrice2");
            matrice2.ForEach(row =>
            {
                row.ForEach(x => Console.Write("{0}, ", x));
                Console.WriteLine();
            });

            Console.WriteLine();
            Console.WriteLine("Matrice Result");
            matriceResult.ForEach(row =>
            {
                row.ForEach(x => Console.Write("{0}, ", x));
                Console.WriteLine();
            });
        }

        private List<List<int>> GetRandomMatrice()
        {
            var matrice = new List<List<int>>();

            for (int i = 0; i < 3; i++)
            {
                matrice.Add(GetRandomList());
            }

            return matrice;
        }

        private List<int> GetRandomList()
        {
            var list = new List<int>();

            for (int i = 0; i < 5; i++)
            {
                list.Add(GetRandomInt());
            }

            return list;
        } 

        private int GetRandomInt()
        {
            return _random.Next(1, 100);
        }
    }
}
