﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tasks
{
    public class Task01
    {
        public void Run()
        {
            var tasks = new List<Task>();

            for (var i = 0; i < 100; i++)
            {
                tasks.Add(new Task(Iteration));
            }

            tasks.ForEach(x => x.Start());

            Task.WhenAll(tasks).Wait();
        }

        private void Iteration()
        {
            for (var i = 0; i < 1000; i++)
            {
                Console.WriteLine("Task # {0}– {1}", Task.CurrentId, i);
            }
        }
    }
}
