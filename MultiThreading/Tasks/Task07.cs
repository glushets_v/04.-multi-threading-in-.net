﻿using System;
using System.Threading.Tasks;

namespace Tasks
{
    public class Task07
    {
        public void Run()
        {
            var tcs = new TaskCompletionSource<bool>();

            var task = new Task(() => Console.WriteLine("Main task"));

            task.ContinueWith(result =>
                {
                    Console.WriteLine(
                        "a. Continuation task should be executed regardless of the result of the parent task");
                    throw new Exception();
                })
                .ContinueWith(result =>
                {
                    Console.WriteLine(
                        "b. Continuation task should be executed when the parent task finished without success");
                    throw new Exception();
                }, TaskContinuationOptions.OnlyOnFaulted)
                .ContinueWith(result =>
                {
                    Console.WriteLine(
                        "c. Continuation task should be executed when the parent task would be finished with fail and parent task thread should be reused for continuation");
                    tcs.TrySetCanceled();
                }, TaskContinuationOptions.OnlyOnFaulted | TaskContinuationOptions.AttachedToParent)
                .ContinueWith(result =>
                {
                    Console.WriteLine("d. Continuation task should be executed outside of the thread pool when the parent task would be cancelled");
                }, TaskContinuationOptions.LazyCancellation | TaskContinuationOptions.ExecuteSynchronously);
            
            task.Start();
        }
    }
}
