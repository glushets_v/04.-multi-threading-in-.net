﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tasks
{
    public class Task02
    {
        private Random _random;

        public Task02()
        {
            _random = new Random();
        }

        public void Run()
        {
            Task.Factory.StartNew(() => GetArray())
            .ContinueWith(antecendent => MultiplyArray(antecendent.Result))
            .ContinueWith(antecendent => SortArr(antecendent.Result))
            .ContinueWith(antecendent => Average(antecendent.Result))
            .Wait();
        }

        private double Average(List<int> sourceArr)
        {
            var average = sourceArr.Average();

            Console.WriteLine("Average: {0}", average);
            return average;
        }

        private List<int> SortArr(List<int> sourceArr)
        {
            Console.WriteLine("Sorted Array");
            var sortedArr = sourceArr.OrderBy(x => x).ToList();

            foreach (var i in sortedArr)
            {
                Console.Write("{0} ", i);
            }
            Console.WriteLine();

            return sortedArr;
        } 

        private List<int> MultiplyArray(List<int> sourceArr)
        {
            Console.WriteLine("Multiply Array:");
            for (int i = 0; i < sourceArr.Count;  i++)
            {
                sourceArr[i] *= GetRandomInt();
            }
            
            sourceArr.ForEach(Console.WriteLine);

            return sourceArr;
        }

        private List<int> GetArray()
        {
            var list = new List<int>();
            Console.WriteLine("Source Array:");

            for (int i = 0; i < 10; i++)
            {
                var item = GetRandomInt();
                Console.WriteLine(item);
                list.Add(item);
            }

            return list;
        }

        private int GetRandomInt()
        {
            return _random.Next(1, 1000);
        }
    }
}
