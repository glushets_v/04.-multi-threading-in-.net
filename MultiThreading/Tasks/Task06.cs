﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Tasks
{
    public class Task06
    {
        private List<int> _list;
        private Object _lockObj = new Object();
        private int _index = 0;
        private bool flag = false;

        public Task06()
        {
            _list = new List<int>();
        }

        public void Run()
        {
            var task1 = new Task(AddItems);

            var task2 = new Task(PrintItems);

            task1.Start();
            task2.Start();
        }

        private void AddItems()
        {
            for (int i = 0; i < 10; i++)
            {
                lock (_lockObj)
                {
                    _index = i;
                    Console.WriteLine("Add {0}", i);
                    _list.Add(i);
                }
            }

            flag = true;
        }

        private void PrintItems()
        {
            var first = 0;
            var last = 0;
            do
            {
                lock (_lockObj)
                {
                    last = _list.Count;
                    for (int i = first; i < last; i++)
                    {
                        Console.WriteLine("Print {0}", _list[i]);
                    }
                    first = last;
                }
            } while (!flag);
        }
    }

}

