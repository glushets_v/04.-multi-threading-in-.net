﻿using System;
using System.Threading;

namespace Tasks
{
    public class Task04
    {
        private int counter = 0;
        private int threadCount = 10;

        public void Run()
        {
            var task = new Thread(() => SomeDo(10));
            task.Start();
            task.Join();
        }

        private int SomeDo(int state)
        {
            counter++;

            if (state == 0)
            {
                return 0;
            }

            var temp = --state;

            if (counter <= threadCount)
            {
                var task = new Thread(() => SomeDo(temp));

                task.Start();
                task.Join();
            }
            
            Console.WriteLine(temp);
            return temp;
        }
    }
}
