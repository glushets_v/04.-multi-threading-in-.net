﻿using System.Linq;
using System.Threading.Tasks;
using Async;
using Async.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class AsyncTests
    {
        [TestMethod]
        public async Task Async01()
        {
            var obj = new Async01();

            var result = await obj.Run(2);
        }

        [TestMethod]
        public async Task Async02_Load()
        {
            var obj = new Async02();

            var urls = new[]
            {
                "http://tut.by"
            };

            var result = await obj.LoadPages(urls);

            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
        }

        [TestMethod]
        public async Task Async02_Cancel_Loading()
        {
            var obj = new Async02();

            var urls = new[]
            {
                "http://tut.by"
            };

            var result = await obj.LoadPages(urls);

            obj.Cancel();
        }

        [TestMethod]
        public async Task Async03_Add_To_Cart()
        {
            var obj = new Async03();

            var add = await obj.AddProduct("bear", 10);

            Assert.AreEqual("Success", add);
            Assert.AreEqual((decimal)125.0, obj._cart.Sum);

            var remove = await obj.RemoveProduct("bear", 5);

            Assert.AreEqual("Success", remove);
            Assert.AreEqual((decimal)62.5, obj._cart.Sum);
        }

        [TestMethod]
        public async Task Async04_Add()
        {
            var obj = new Async04();

            var user = new User
            {
                Id = 1,
                FirstName = "FirstName",
                LastName = "LastName",
                Age = 30
            };

            await obj.AddAsync(user);

            Assert.AreEqual(1, obj._users.Count);
        }

        [TestMethod]
        public async Task Async04_Update()
        {
            var obj = new Async04();

            var user = new User
            {
                Id = 1,
                FirstName = "FirstName",
                LastName = "LastName",
                Age = 30
            };

            await obj.AddAsync(user);

            var updateUser = new User
            {
                Id = 1,
                FirstName = "Vasya",
                LastName = "Pupkin",
                Age = 34
            };

            var result = await obj.UpdateAsync(updateUser);

            Assert.IsTrue(result);

            var storeduser = obj._users.FirstOrDefault(x => x.Id == 1);
            
            Assert.IsNotNull(storeduser);
            Assert.AreEqual(storeduser.FirstName, updateUser.FirstName);
            Assert.AreEqual(storeduser.LastName, updateUser.LastName);
            Assert.AreEqual(storeduser.Age, updateUser.Age);
        }

        [TestMethod]
        public async Task Async04_Delete()
        {
            var obj = new Async04();

            var user = new User
            {
                Id = 1,
                FirstName = "FirstName",
                LastName = "LastName",
                Age = 30
            };

            await obj.AddAsync(user);

            var result = await obj.DeleteUser(user.Id);

            Assert.IsTrue(result);
            Assert.AreEqual(0, obj._users.Count);
        }

        [TestMethod]
        public async Task Async04_Get()
        {
            var obj = new Async04();

            var user = new User
            {
                Id = 1,
                FirstName = "FirstName",
                LastName = "LastName",
                Age = 30
            };

            await obj.AddAsync(user);

            var result = await obj.GetByIdAsync(user.Id);

            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, user.Id);
        }
    }
}
