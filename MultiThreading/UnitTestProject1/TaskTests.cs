﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tasks;

namespace UnitTestProject1
{
    [TestClass]
    public class TaskTests
    {
        [TestMethod]
        public void Task01()
        {
            var task = new Task01();

            task.Run();
        }

        [TestMethod]
        public void Task02()
        {
            var task02 = new Task02();

            task02.Run();
        }

        [TestMethod]
        public void Task03()
        {
            var task03 = new Task03();

            task03.Run();
        }

        [TestMethod]
        public void Task04()
        {
            var task04 = new Task04();

            task04.Run();
        }

        [TestMethod]
        public void Task05()
        {
            var task05 = new Task05();

            task05.Run();
        }

        [TestMethod]
        public void Task06()
        {
            var task06 = new Task06();

            task06.Run();
        }

        [TestMethod]
        public void Task07()
        {
            var task07 = new Task07();

            task07.Run();
        }
    }
}
