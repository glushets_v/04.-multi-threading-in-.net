﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MultyThreding.Console
{
    class Program
    {
        private int thredCount = 5;

        static void Main(string[] args)
        {
            var sourceData = new SourceData();

            var allItems = sourceData.GetAllItems();

            List<Thread> listThreads = allItems.Select(item => new Thread(() => sourceData.ProcessDataItem(item))).ToList();
            

            listThreads.ForEach(x => x.Start());



            Thread thread1 = new Thread(() => { });
            Thread thread2 = new Thread(SomeMeth);
            Thread thread3 = new Thread(() => sourceData.ProcessDataItem(10));
        }

        public static void DoParallel()
        {
            var sourceData = new SourceData();

            var allItems = sourceData.GetAllItems();

            Parallel.ForEach(allItems, item => sourceData.ProcessDataItem(item));
        }

        public static void DoTasks()
        {
            var sourceData = new SourceData();

            var allItems = sourceData.GetAllItems();

            var taskCount = 5;
            var tasks = new List<Task>();

            foreach (var item in allItems)
            {
                tasks.Add(Task.Factory.StartNew(() => sourceData.ProcessDataItem(item)));

                if (tasks.Count >= taskCount)
                {
                    Task.WhenAny(tasks).Wait();
                    tasks.RemoveAll(task => task.Status != TaskStatus.Running);
                }
            }
            Task.WaitAll(tasks.ToArray());
        }


        public static void SomeMeth()
        {
            
        }
    }
}
